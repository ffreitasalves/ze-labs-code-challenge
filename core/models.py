from django.contrib.gis.db import models


# Create your models here.
class PDV(models.Model):
    trading_name = models.CharField(max_length=200)
    owner_name = models.CharField(max_length=80)
    document = models.CharField(max_length=14, unique=True)
    coverage_area = models.MultiPolygonField()
    address = models.PointField()

    def __str__(self):
        return self.trading_name
