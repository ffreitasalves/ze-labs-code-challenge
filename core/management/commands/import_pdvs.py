from django.core.management.base import BaseCommand

class Command(BaseCommand):
    help = 'Import all PDVs to Database'


    def handle(self, *args, **options):
        from utils import load_pdvs

        load_pdvs()