from core.models import PDV
from django.db import IntegrityError
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase


class PDVAPITestCase(APITestCase):
    fixtures = ['pdv_fixtures.json']

    def test_get_pdv_by_id_one(self):
        response = self.client.get('/pdv/1/')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['trading_name'], "Adega Osasco")

    def test_get_nearest_pdv(self):
        response = self.client.get('/pdv/?lng=-44.012478&lat=-19.887215')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0]['trading_name'], "Adega do Joao")
        self.assertEqual(response.data[0]['id'], 51)


class PDVModelTestCase(TestCase):
    fixtures = ['pdv_fixtures.json']

    def test_pdv_cnpj_document_is_unique(self):
        """ Try to insert a PDV with a  CNPJ document already used"""

        existing_pdv = PDV.objects.last()
        new_pdv = PDV()

        new_pdv.trading_name = "Foo"
        new_pdv.owner_name = "Bar"
        new_pdv.document = existing_pdv.document
        new_pdv.coverage_area = existing_pdv.coverage_area
        new_pdv.address = existing_pdv.address

        with self.assertRaises(IntegrityError):
            new_pdv.save()

    def test_trading_name_not_null(self):
        existing_pdv = PDV.objects.last()

        with self.assertRaises(IntegrityError):
            existing_pdv.trading_name = None
            existing_pdv.save()

    def test_owner_name_not_null(self):
        existing_pdv = PDV.objects.last()

        with self.assertRaises(IntegrityError):
            existing_pdv.owner_name = None
            existing_pdv.save()

    def test_document_not_null(self):
        existing_pdv = PDV.objects.last()

        with self.assertRaises(IntegrityError):
            existing_pdv.document = None
            existing_pdv.save()

    def test_coverage_area_not_null(self):
        existing_pdv = PDV.objects.last()

        with self.assertRaises(IntegrityError):
            existing_pdv.coverage_area = None
            existing_pdv.save()

    def test_address_not_null(self):
        existing_pdv = PDV.objects.last()

        with self.assertRaises(IntegrityError):
            existing_pdv.address = None
            existing_pdv.save()

    def test_trading_name_str(self):
        existing_pdv = PDV.objects.last()

        self.assertEqual(existing_pdv.trading_name, existing_pdv.__str__())