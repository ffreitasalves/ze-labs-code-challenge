from rest_framework import serializers
from .models import PDV


class PDVSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = PDV
        fields = (
            'id',
            'trading_name',
            'owner_name',
            'document',
            'coverage_area',
            'address'
        )
