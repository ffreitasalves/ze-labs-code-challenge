# Generated by Django 2.2.1 on 2019-05-15 13:21

import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PDV',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('trading_name', models.CharField(max_length=200)),
                ('owner_name', models.CharField(max_length=80)),
                ('document', models.CharField(max_length=14, unique=True)),
                ('coverage_area', django.contrib.gis.db.models.fields.MultiPolygonField(srid=4326)),
                ('address', django.contrib.gis.db.models.fields.PointField(srid=4326)),
            ],
        ),
    ]
