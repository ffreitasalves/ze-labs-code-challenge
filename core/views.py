from rest_framework import viewsets
from rest_framework.decorators import action
from .serializers import PDVSerializer
from .models import PDV


class PDVViewSet(viewsets.ModelViewSet):
    queryset = PDV.objects.all()
    serializer_class = PDVSerializer

    def get_queryset(self):
        from django.contrib.gis.geos import GEOSGeometry, Point
        from django.contrib.gis.db.models.functions import Distance

        qs = self.queryset

        long = float(self.request.query_params.get('lng', 0))
        lat = float(self.request.query_params.get('lat', 0))


        if long and lat:

            base_point = Point(long, lat)
            base_point = GEOSGeometry(base_point, srid=4326)

            qs = self.queryset.filter(
                coverage_area__contains=base_point
            ).annotate(
                distance=Distance('address', base_point)
            ).order_by('distance')

        return qs
