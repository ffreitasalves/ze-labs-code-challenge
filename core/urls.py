from django.urls import path, include
from rest_framework import routers
from .views import PDVViewSet

router = routers.DefaultRouter()
router.register('pdv', PDVViewSet)


urlpatterns = [
    path('', include(router.urls)),
]
