# ze-labs-code-challenge

# Installation

git clone the repo:

```
git clone git@gitlab.com:ffreitasalves/ze-labs-code-challenge.git
cd ze-labs-code-challenge
source .envrc
docker-compose up
```

it will automatically create all PDVs from `pdvs.json`
and run the app at [http://localhost:8000](http://localhost:8000)


# Listing all PDVs
GET https://ze-labs-prod.herokuapp.com/pdv/

## Filtering against Lat and Long 
Get the nearest PDV that covers a Point
GET https://ze-labs-prod.herokuapp.com/pdv/?lng=-44.012178&lat=-19.887100

## Access PDV by id
GET https://ze-labs-prod.herokuapp.com/pdv/1/  # id=1, for instance


# Running Tests (with coverage)
`docker-compose run --rm ze_web bash run_tests.sh`

# Deploy to Heroku
This app is running at [https://ze-labs-prod.herokuapp.com/](https://ze-labs-prod.herokuapp.com/)

There is a Continuous Integration tool configured here,

Just open a PR to master or `git push` to it (if you have permission) and GitLab CI will deploy it directly to heroku.

![pipeline](https://gitlab.com/ffreitasalves/ze-labs-code-challenge/badges/master/pipeline.svg)