import os
from django.core.exceptions import ImproperlyConfigured


def get_env_variable(var_name, default=None):  # pragma: no cover
    try:
        return os.environ[var_name]
    except KeyError:
        if not default:
            error_msg = "Set the %s environment variable" % var_name
            raise ImproperlyConfigured(error_msg)
        return default


def load_pdvs(file_path='pdvs.json'):
    """
    Load all data from filepath do database
    """
    import json
    import re
    from django.contrib.gis.geos import GEOSGeometry
    from django.db import DataError
    from core.models import PDV

    with open(file_path, 'r') as file_pdvs:
        content = file_pdvs.read()

    counter = 0
    success_counter = 0
    for pdv in json.loads(content)['pdvs']:

        counter += 1
        print("Reading PDV {}".format(pdv['id']))
        try:
            new_pdv = PDV()
            new_pdv.id = pdv['id']
            new_pdv.trading_name = pdv['tradingName']
            new_pdv.owner_name = pdv['ownerName']
            new_pdv.document = re.sub(r"\D", "", pdv['document'])
            new_pdv.coverage_area = GEOSGeometry(pdv['coverageArea'].__str__())
            new_pdv.address = GEOSGeometry(pdv['address'].__str__())
            new_pdv.save()

            print("Including new PDV {}".format(new_pdv.trading_name))
            success_counter +=1

        except DataError:
            print("warning: PDV {} not imported".format(pdv['id']))

    print("{} PDVs imported from {} submitted".format(success_counter, counter))
